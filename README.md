# Deployment Test

Architecture diagram
====================

![Alt text](Diagram.png)
<img src="Diagram.png">


## General notes
The following repository provides a draft about a test related to how to deploy a custom system to provide geographic information.
The interviewer ask to provide a solution that meets the principles of Continuos Integration and fast deployment.

- To achive that architecture the solution proposed is based around containers and Docker. It is providen some Dockerfiles examples to build
the applications described. The dockerfiles are unit files that can automate the building process of an application, and then provide it
as a container. With this approach, it is easy to automate the building process when there is any commit in our upstream server.
- Also with containers it is easy to deploy different environments, and the software deployed in test/production would be the same, but being
able to customize both deployments with the arguments submited with the api container.
- Furthermore, the container format makes easy to scale the number of instances of any of the microservice containers. With the code it is submited a 
`docker-compose.yml` file that describes a very basic microservice integration, but it's also quite powerfull because it allows us to scale the instance number
of any service running simple commands as `docker-compose scale api=2 geoserver=5`.
- Lastly, with Docker swarm this approach could be used to orchestrate and deploy these services across multiple servers, providing a simple way to deploy 
multi-server applications, and also providing a serving iscovery mechanism based on DNS, very easy to addapt to any application. And also Swarm
will monitor the services so, in the case an app/node fails, that cointainers will be rescheduled to other nodes.


In `docker-compose.yml` we have defined an example of how to deploy an scenario. This can also be customized with custom parameters (production/test) 
and a different network (with different ip ranges if desired), to deploy the different scenarios asked.

Some other notes:
=================
- For geotiff file storage, we supposed every node that can run the container GeoServer would have a NFS volume available in nfs://nfs.geoserver.net:/geotiff_data.
This volume has be also defined in `docker-compose.yml`, specifying that the volume driver to use is nfs and also you can provide here any login requirement. 
Thus, the nfs volume will be handle correctly inside the containers and the nfs locks will work propperly.
With the requirements given (5K files with size upto 300GB), and supposed that it won't need to have a high level of write-access seems a good solution for storing the files. 
For more advanced solution for this layer we could though in other filestorage systems as GlusterFS.
- The Loadbalancing for GeoServer can be performed directly with Docker. There are different approaches to this:
	* From Docker 1.12 and the new service definition, we can define the port where a service will be deployed (in our `docker-compose.yml` example, 80 for api service and 8080 for geoserver). Any node belonging to our swarm cluster will 
	redirect any query to these ports to a container providing that service. Thus, you can configure your load-balancer (F5, or custom cloud solution) with the list of all nodes of your swarm and the corresponding port, and it will balance the traffic easily.
	* From inside any container, Docker will provide a DNS service that can be used also as load-balancing (asking for example for the net-alias)
	* It's also possible to build the discovery based on Docker API, configuring your load-balancer to ask to the information.
	* Lastly, we could also use a third solution to provide the service discovery. Hashicorp Consul is a complete solution that can be easily configured to automatically discover your docker contaienrs and services (using registrator), and
	the using Consul to provide the information to the Load Balancer. Also Consul could be used as a Key-Value store if we do not want to pass the arguments to api.jar directly.
